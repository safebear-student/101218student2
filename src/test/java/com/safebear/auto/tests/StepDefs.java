package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class StepDefs {

    WebDriver driver;
    LoginPage loginPage;
    ToolsPage toolsPage;

    @Before
    public void setUp() {

        driver = Utils.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);
    }

    @After
    public void tearDown() {
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();

    }

    @Given("^I navigate to the login page$")
    public void i_navigate_to_the_login_page() {
        driver.get(Utils.getUrl());
        Assert.assertEquals("Login Page", loginPage.getPageTitle(), "we're not on the login page or its title has changed");

    }

    @When("^I enter the login details for '(.+)'$")
    public void i_enter_the_login_details_for_a_User(String userType) {
        switch (userType) {
            case "invalidUser":
                loginPage.enterUsername("attacker");
                loginPage.enterPassword("dontletmein");
                loginPage.clickSubmit();
                break;

            case "validUser":
                loginPage.enterUsername("tester");
                loginPage.enterPassword("letmein");
                loginPage.rememberMe();
                loginPage.clickSubmit();
                break;

            default:
                Assert.fail("Test data is incorrect - check values for valid and invalid User");
                break;

        }

    }

    @Then("^I can see the following message: '(.+)'$")
    public void i_can_see_the_following_message(String validationMessage) {
        switch (validationMessage) {
            case "Username or Password is incorrect":
                Assert.assertTrue(loginPage.failedMessage().contains(validationMessage));
                break;
            case "Login Successful":
                Assert.assertTrue(toolsPage.isSuccessfulMessage().contains(validationMessage));
                break;
            default:
                Assert.fail("The test data is wrong");
                break;

        }

    }
}