package com.safebear.auto.tests;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
        plugin = {
                "pretty",
                "html:target/cucumber-reports/cucumber-pretty",
                "json:target/cucumber-reports/CucumbertestReport.json",
                "return:target/cucumber-reports/rerun.txt "},
        tags = "~@to-do",
        glue = "com.safebear.auto.tests",
        features = "classpath:toolslist.features/login.feature"
)

public class RunCukes extends AbstractTestNGCucumberTests {
}